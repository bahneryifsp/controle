// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDsvOO1OXciKJoW1pVYARio6cgNd8Ts408',
    authDomain: 'controle-bahneryifsp.firebaseapp.com',
    databaseURL: 'https://controle-bahneryifsp.firebaseio.com',
    projectId: 'controle-bahneryifsp',
    storageBucket: 'controle-bahneryifsp.appspot.com',
    messagingSenderId: '800971712357',
    appId: '1:800971712357:web:3936cf44a2012a8703238c',
    measurementId: 'G-VPMPP0T5KM'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
